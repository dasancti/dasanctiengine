﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class TextAnimationController : MonoBehaviour {

    [SerializeField]
    private string text;
    [SerializeField]
    [Range(0,100)]
    [Tooltip("Percentage of characters from total length")]
    private float length;
    private int WantedLength
    {
        get
        {
            return (this.text.Length / 100) * (int)this.length;
        }
    }

    private Text textToAnimate;

    void Awake()
    {
        this.textToAnimate = this.GetComponent<Text>();
        if(this.textToAnimate == null)
        {
            this.textToAnimate = this.gameObject.AddComponent<Text>();
        }

        Assert.IsNotNull(this.textToAnimate);
        if (string.IsNullOrEmpty(this.text))
            this.text = this.textToAnimate.text;
        Assert.IsFalse(string.IsNullOrEmpty(this.text));

        this.textToAnimate.text = text;
    }

    // Use this for initialization
    void OnEnable()
    {
        if (this.length > 0 && this.length <= this.text.Length)
            this.textToAnimate.text = this.text.Substring(0, this.WantedLength);
        else
            this.textToAnimate.text = this.text;
    }

    void Update()
    {
        if (this.length > 0 && this.length <= this.text.Length)
            this.textToAnimate.text = this.text.Substring(0, this.WantedLength);
        else
            this.textToAnimate.text = this.text;
    }

}
