﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
#if UNITY_EDITOR
using UnityEditor;
using System.Xml.Linq; // System.Xml.Linq.dll prejmenovat
using System.IO;
#endif

using DaSanctiEngine;

namespace BigFruits
{

    /**
     * Třida pro nastavení proměných pro ulehčení debugování a testování hry.
     * Pr.:  if( DebugConfig.Asset.allLevelsUnlocked ) return true;
     */

    public class GameDebug : ScriptableAsset<GameDebug>
    {
#if UNITY_EDITOR
        [Serializable]
        public class EditorClass
        {
            [Header("Preferences")]
            [Space(10)]
            public bool imCool = true;
        }

        [Space(10)]
        public EditorClass editor;

        public void ClearEditor()
        {
            this.editor.imCool = true;
        }

        public void SetToImNotCool()
        {
            // zatim stejne
            this.editor.imCool = false;
        }

        [MenuItem("GameDebug/Select Debug Config %#d", false, 0)]
        private static void SelectSettings()
        {
            // EditorApplication.ExecuteMenuItem("Build Settings...%#b");
            Selection.activeObject = Asset;
        }
#endif
    }



#if UNITY_EDITOR
        [CustomEditor(typeof(GameDebug))]
    public class GameDebugEditor : Editor
    {
        void OnEnable()
        {

        }

        public override void OnInspectorGUI()
        {
            float lb = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 200f;

            GameDebug dbg = (GameDebug)this.target;

            if (!Application.isPlaying)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();

                if (GUILayout.Button("Set To Im Not Cool", GUILayout.MaxWidth(180)))
                    dbg.SetToImNotCool();
                
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            base.OnInspectorGUI();
        }
    }

    // TOOLS
    public class DaSanctiTools
    {
        
    }
#endif
}