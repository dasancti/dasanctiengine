﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace DaSanctiEngine
{

    /// <summary>
    /// Auto serialized asset.
    /// <para>
    /// Vhodne na vytvaranie (statickych) configuracii, ktore sa daju velmi rychlo pozmenit v editore teda bez potreby upravovat kod, kompilovat atd. *Prikladne pouzite - <see cref="AGNotifications"/>
    /// </para>
    /// <para>
    /// Funguje ako singleton v podobe assetu. Teda je potrebne aby bol asset vytvoreny v editore. Teda odporucam
    /// po dedeni pridat staticku funkciu s tlacitkom v Unity Menu na vytvorenie/selectnutie assetu.
    /// <code>
    /// #if UNITY_EDITOR
    ///     [MenuItem("DaSanctiEngine/Select/Select Nazev")]
    ///     static void SelectSettings()
    ///     {
    ///         Selection.activeObject = Asset;
    ///    }
    /// #endif
    /// </code>
    /// </para>
    /// </summary>
    public class ScriptableAsset<T> : ScriptableObject where T : ScriptableObject
    {
        protected static T _asset;

        public static T Asset
        {
            get
            {
                if (_asset == null)
                {
                    _asset = (T)Resources.Load("DaSanctiGames/" + typeof(T).Name, typeof(T));

#if UNITY_EDITOR
                    if (_asset == null)
                    {
                        CreateAsset();
                    }
#endif

                }
                return _asset;
            }
        }

        public static T AssetAtPath(string pPath)
        {
            if (string.IsNullOrEmpty(pPath))
            {
                return Asset;
            }

            if (_asset == null)
            {
                pPath = pPath.Trim('/');
                _asset = (T)Resources.Load(pPath + "/" + typeof(T).Name, typeof(T));
#if UNITY_EDITOR
                if (_asset == null)
                {
                    CreateAssetAtPath(pPath);
                }
#endif

            }
            return _asset;
        }

        public static T GetInstance()
        {
            return Asset;
        }

        public static T Instance
        {
            get
            {
                return GetInstance();
            }
        }

#if UNITY_EDITOR
        public static void CreateAsset()
        {

            //If the settings asset doesn't exist, then create it. We require a resources folder
            if (!Directory.Exists(Application.dataPath + "/Game"))
            {
                Directory.CreateDirectory(Application.dataPath + "/Game");
            }
            if (!Directory.Exists(Application.dataPath + "/Game/Resources"))
            {
                Directory.CreateDirectory(Application.dataPath + "/Game/Resources");
            }
            if (!Directory.Exists(Application.dataPath + "/Game/Resources/DaSanctiGames"))
            {
                Directory.CreateDirectory(Application.dataPath + "/Game/Resources/DaSanctiGames");
                Debug.LogWarning("Game/Resources/DaSanctiGames folder is required to store AutoSerializedAsset. It has been created.");
            }

            var asset = ScriptableObject.CreateInstance<T>();

            string uniquePath = "Assets/Game/Resources/DaSanctiGames/" + typeof(T).Name + ".asset";
            AssetDatabase.CreateAsset(asset, uniquePath);
            AssetDatabase.SaveAssets();

            //save reference
            _asset = asset;

        }
#endif

#if UNITY_EDITOR
        public static void CreateAssetAtPath(string pPath)
        {
            string[] directories = pPath.Split('/');
            string currentPath = Application.dataPath + "/Resources";

            if (directories.Length > 0)
            {
                for (int i = 0; i < directories.Length; i++)
                {
                    currentPath += "/" + directories[i];
                    if (!Directory.Exists(currentPath))
                    {
                        Directory.CreateDirectory(currentPath);
                        if (i == directories.Length - 1)
                        {
                            Debug.LogWarning("file Assets" + currentPath.Replace(Application.dataPath, "") + "/" + typeof(T).Name + ".asset folder is required to store AutoSerializedAsset. It has been created.");
                        }
                    }
                }
            }
            string uniquePath = "Assets" + currentPath.Replace(Application.dataPath, "") + "/" + typeof(T).Name + ".asset";
            var asset = ScriptableObject.CreateInstance<T>();

            AssetDatabase.CreateAsset(asset, uniquePath);
            AssetDatabase.SaveAssets();

            //save reference
            _asset = asset;
        }
#endif
    }

}