﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace DaSanctiEngine
{
    public class XMLTextInfo
    {
        private Dictionary<string, string> myTexts = new Dictionary<string, string>();

        public void LoadInfo(string name)
        {
            XDocument doc = null;

            try
            {
                TextAsset ass = Resources.Load<TextAsset>("Configs/" + name);
                if (ass != null)
                {
                    string cx = ass.text.Trim();

                    ass = null;
                    doc = XDocument.Parse(cx);
                }
            }
            catch (System.Xml.XmlException e)
            {
                Debug.LogError("XMLTextInfo LoadInfo " + name + " \n" + e.Message);
            }

            IEnumerable<XElement> allTexts = doc.Root.Elements("text");

            foreach (XElement text in allTexts)
            {
                string textName = text.StringAttribute("name", null);
                Assert.IsFalse(string.IsNullOrEmpty(textName));

                string textValue = text.StringAttribute("text", null);
                Assert.IsFalse(string.IsNullOrEmpty(textValue));

                myTexts[textName] = textValue;
            }
        }

        public string GetText(string name)
        {
            if (!myTexts.ContainsKey(name))
                return null;
            return myTexts[name];
        }
    }
}
