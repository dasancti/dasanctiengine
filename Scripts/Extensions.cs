﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

namespace DaSanctiEngine
{
    public static class Extensions
    {

        #region XML extensions
        public static int IntAttribute(this XElement xel, string attrName, int def)
        {
            XAttribute att = xel.Attribute(attrName);
            if (att != null)
                return (int)att;
            return def;
        }

        public static bool BoolAttribute(this XElement xel, string attrName, bool def)
        {
            XAttribute att = xel.Attribute(attrName);
            if (att != null)
                return (int)att == 1;
            return def;
        }

        public static double DoubleAttribute(this XElement xel, string attrName, double def)
        {
            XAttribute att = xel.Attribute(attrName);
            if (att != null)
                return (double)att;
            return def;
        }

        public static string StringAttribute(this XElement xel, string attrName, string def)
        {
            XAttribute att = xel.Attribute(attrName);
            if (att != null)
                return (string)att;
            return def;
        }

        public static float FloatAttribute(this XElement xel, string attrName, float def)
        {
            XAttribute att = xel.Attribute(attrName);
            if (att != null)
                return (float)att;
            return def;
        }
        #endregion
    }
}
